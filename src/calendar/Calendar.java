package calendar;

import java.sql.*;
import java.sql.Date;
import java.util.*;

public class Calendar
{
	List<Appointment> appointments;
	
	private Calendar()
	{
		appointments = new LinkedList<Appointment>();
	}
	
	private void add_appointment(Date d, Time st, Time et, String n)
	{
		Appointment a = new Appointment(d, st, et, n);
		this.appointments.add(a);
	}
	
}
