package calendar;

import java.sql.*;

public class Appointment 
{
	private Date date;
	private Time start_time;
	private Time end_time;
	private String name;
	
	Appointment(Date d, Time st, Time et, String n)
	{
		date = d;
		start_time = st;
		end_time = et;
		name = n;
	}
}
